#define _POSIX_C_SOURCE 200112L
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#include <string.h>

#include <sys/types.h>
#include <gpiod.h>

int main()
{
int retval;

/* root user */

retval = setuid(0);
if (retval != 0)
   printf("%s %3d: setuid: %s\n", __FILE__, __LINE__, strerror(errno));

/* check what modules are loaded */

retval = system("/bin/lsmod");
printf("%s %3d: /bin/lsmod: %s\n", __FILE__, __LINE__, strerror(errno));

/* unload gpio_keys_polled */

retval = system("/sbin/modprobe -r gpio_keys_polled");
printf("%s %3d: /sbin/modprobe -r gpio_keys_polled: %s\n", __FILE__, __LINE__, strerror(errno));

/* check what modules are loaded */

retval = system("/bin/lsmod");
printf("%s %3d: /bin/lsmod: %s\n", __FILE__, __LINE__, strerror(errno));

/* load gpio_keys_polled */

retval = system("/sbin/modprobe gpio_keys_polled");
printf("%s %3d: /sbin/modprobe gpio_keys_polled: %s\n", __FILE__, __LINE__, strerror(errno));

/* check what modules are loaded */

retval = system("/bin/lsmod");
printf("%s %3d: /bin/lsmod: %s\n", __FILE__, __LINE__, strerror(errno));

return 0;

}


